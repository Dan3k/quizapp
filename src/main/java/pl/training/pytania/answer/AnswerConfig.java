package pl.training.pytania.answer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AnswerConfig {

    @Bean
    public AnswerService answerService(AnswerRepository answerRepository) {
        return new AnswerService(answerRepository);
    }




}
