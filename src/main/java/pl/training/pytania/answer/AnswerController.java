package pl.training.pytania.answer;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/answer")
@RequiredArgsConstructor
public class AnswerController {

    @NonNull
    private AnswerService answerService;

    @RequestMapping(method = RequestMethod.POST)
    public Answer createAnswer(Answer answer) {
        return answerService.save(answer);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Answer getAnswerById(@PathVariable("id") Long id) {
        return answerService.findById(id).get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteAnswer(Long id) {
        answerService.deleteById(id);
        return ResponseEntity.ok().body("Answer with id: " + id + "deleted");
    }


}
