package pl.training.pytania.answer;

import lombok.Data;
import pl.training.pytania.question.Question;

import javax.persistence.*;

@Entity
@Data
public class AnswerGiven {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    private Question question;

    @OneToOne
    private Answer answer;


}
