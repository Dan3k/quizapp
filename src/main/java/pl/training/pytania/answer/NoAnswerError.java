package pl.training.pytania.answer;

public class NoAnswerError extends Exception{

    public NoAnswerError() {
        super("No answer");
    }
}
