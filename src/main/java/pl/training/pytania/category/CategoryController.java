package pl.training.pytania.category;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {

    private static int id = 0;

    @NonNull
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllCategories() {
        List<Category> categories = categoryService.findAll();
        return ResponseEntity.ok().body(categories);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createCategory() {
        categoryService.save(new Category("kategoria testowa: " + ++id));
        return ResponseEntity.ok().body("Dodano kategorię");
    }

}
