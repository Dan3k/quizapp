package pl.training.pytania.question;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.training.pytania.answer.Answer;
import pl.training.pytania.category.Category;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String question;

    @ManyToOne
    private Category category;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id")
    private Set<Answer> answers;


}
