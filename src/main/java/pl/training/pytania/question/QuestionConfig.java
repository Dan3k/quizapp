package pl.training.pytania.question;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuestionConfig {

    @Bean
    QuestionService questionService(QuestionRepository questionRepository) {
        return new QuestionService(questionRepository);
    }
}
