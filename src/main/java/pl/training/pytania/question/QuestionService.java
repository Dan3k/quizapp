package pl.training.pytania.question;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
public class QuestionService {

    @NonNull
    QuestionRepository questionRepository;

    public Question save(Question question) {
        return questionRepository.save(question);
    }

    @Transactional
    public Optional<Question> findById(Long id) {
        return questionRepository.findById(id);
    }
}
