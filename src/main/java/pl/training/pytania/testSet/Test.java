package pl.training.pytania.testSet;

import lombok.Data;
import pl.training.pytania.question.Question;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Test {

    @Id
    @GeneratedValue
    private Long id;

    private String testName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "test_questions",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "question_id")}
    )
    private List<Question> questions;

}
