package pl.training.pytania.testSet;

import lombok.Data;
import pl.training.pytania.answer.AnswerGiven;
import pl.training.pytania.user.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "instance")
public class TestInstance {

    @Id
    @GeneratedValue
    private Long id;

    private Date testDate;

    @OneToMany
    @JoinColumn(name = "instance_id")
    private List<AnswerGiven> answerGiven;

    @ManyToOne()
    @JoinColumn(name = "test_id")
    private Test test;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;


}
