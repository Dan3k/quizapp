package pl.training.pytania.testSet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TestInstanceRepository extends JpaRepository<TestInstance, Long> {
}
