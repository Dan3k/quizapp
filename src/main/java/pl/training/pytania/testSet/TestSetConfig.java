package pl.training.pytania.testSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestSetConfig {

    @Bean
    public TestSetService testSetService(TestSetRepository testSetRepository) {
        return new TestSetService(testSetRepository);
    }

}
