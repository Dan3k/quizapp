package pl.training.pytania.testSet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TestSetRepository extends JpaRepository<Test, Long> {
}
